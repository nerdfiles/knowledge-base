= Philosophy =

== Politics ==

=== Republicanism ===

  S is free iff
    freedom of speech,
    freedom of conscience (religion), and
    the right to vote

=== Anarchism ===

  System cannot ensure veritability of itself,
  can only enforce changes and laws (Rule of Recognition).

=== Practical questions for Republicans ===

1. Does your system promote free-riders?

2. Does the Leveling of society stifle competition, initiative and creative thinking?

3. Does your system foster interest group politics?

4. Is your state vulnerable to excessive taxation?

5. Is your state vulnerable to excessive bureaucracy?

6. Does accountability become increasingly difficult as your state grows?

=== Practical questions for Anarchists ===

1. Your libertarianism, which compares income taxation to forced labor, fails to 
acknowledge the need for a guarantee of some baseline level of social security 
and educational benefits to all citizens. Can you somehow still ensure the 
continued loyalty of the poor to the state?

2. No federally insured bank deposits (FDIC)?

3. No food and drug inspection?

4. No pollution regulations?

5. No enforceable labor laws – 40 hr work week for example?

6. Is polygamy allowed?

7. Are addictive drugs allowed?

8. No workplace safety regulations or workers compensation laws?

= Generated Links =
  - [[Articles]]
  - [[Blockchains]]
  - [[Class]]
  - [[Design Patterns]]
  - [[Ideas]]
  - [[Inspiration]]
  - [[Music]]
  - [[Philosophy]]
  - [[Projects]]
  - [[Research]]
  - [[The Beast]]
  - [[Websites]]
  - [[index]]
